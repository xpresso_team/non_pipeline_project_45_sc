# Calculator
class Calculator:
    """
    A Simple Calculator
    """
    def __init__(self, a:int, b:int):
        """
        Initialization
        :param a:
        :param b:
        """
        self.a = a
        self.b = b

    def add(self):
        """

        :return: (a+b)
        """
        return self.a + self.b


if __name__ == "__main__":
    calc = Calculator(3, 6)
    sum = calc.add()
    print(sum)